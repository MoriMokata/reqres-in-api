package entity

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_naem"`
	Avatar    string `json:"avatar"`
}

type Support struct{
	Url string `json:"url"`
	Text string `json:"text"`
}