package entity

import (
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var db *gorm.DB

func DB() *gorm.DB {
	return db
}

func SetupDatabase () {
	dsn := "host=192.168.1.2 user=postgres password=root dbname=postgres port=5432 sslmode=disable TimeZone=UTC+7"
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}

	database.AutoMigrate(&User{})
	db = database


	User1 := User{
		Email : "Chalermkeit@gmailcom",
		FirstName : "Chalermkiet",
		LastName : "Kongkapan",
		Avatar : "https://media.wired.co.uk/photos/60c8730fa81eb7f50b44037e/3:2/w_3329,h_2219,c_limit/1521-WIRED-Cat.jpeg",
	}
	
	db.Model(&User{}).Create(&User1)
}


