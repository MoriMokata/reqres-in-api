# reqres-in-api

## Getting started

## สั่ง download dependency ด้วยคำสั่ง

```sh
go mod tidy
```

### สั่ง compile โปรแกรม backend ด้วยคำสั่ง

```sh
go build -o main.exe main.go
```

### รันโปรแกรมโดยการรันคำสั่ง main

```sh
.\main.exe
```

หรือ

```sh
./main บน Linux และ macOS
```

