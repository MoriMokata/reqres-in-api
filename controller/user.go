package controller

import (
	"net/http"
	"reqres-in/entity"

	"github.com/gin-gonic/gin"
)

func GetUser(c *gin.Context) {
	id := c.Param("id")
	var user entity.User

	if err := entity.DB().Raw("SELECT * FROM users WHERE id = ?", id).Scan(&user).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	support := entity.Support{
		Url:  "https://reqres.in/#support-heading",
		Text: "To keep ReqRes free, contributions towards server costs are appreciated!",
	}

	c.JSON(http.StatusOK, gin.H{"data": user, "support": support})
}


func PostUser(c *gin.Context) {
	var user entity.User
	if err := c.ShouldBindJSON(&user); err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error" : err.Error()})
		return
	}
	if err := entity.DB().Create(&user).Error; err!=nil{
		c.JSON(http.StatusBadRequest, gin.H{"error" : err.Error()})
		return
	}
	c.JSON(http.StatusOK, gin.H{"data":user})
}