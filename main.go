package main

import (
	"reqres-in/entity"
	"reqres-in/controller"
	"github.com/gin-gonic/gin"
)

func main() {
	entity.SetupDatabase()
	r := gin.Default()

	api := r.Group("/api")
	{
		userApi := api.Group("/users")
		userApi.POST("", controller.PostUser)
		userApi.GET("/:id",controller.GetUser)
		
	}
	r.Run()
}